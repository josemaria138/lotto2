package pl.akademiakodu.lotto;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {
/*
@ResponseBody el resultado que obtenemos es String
 */

    @ResponseBody
    @GetMapping("/")  // po tej scziezcze wywolana jest metoda hello
    public String hello() {
        return "Witaj swiecie";
    }

    @ResponseBody
    @GetMapping("/bye")  // ponemos la direccion despues de la barra
    public String bye() {
        return "Do widzenia";
    }

    @GetMapping("/welcome")
    public String welcome() {
        return "witam"; // aqui devuelve html
        // resources/templates/witam.html
    }


    @GetMapping("/product")
    public String helloProduct() {
        return "produkt"; // aqui devuelve html
        // resources/templates/produkt.html
    }
    @GetMapping("/lotto")
    public String generateLotto(ModelMap map) {
        LottoGenerator lottoGenerator=new LottoGenerator();
        map.put("numbers",lottoGenerator.generate());

        return "lotto";
    }

}


